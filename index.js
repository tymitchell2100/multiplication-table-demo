// output the header row
document.write('<span class="cell header">&times;</span>');
for (let i = 1; i <= 10; i++) {
    document.write('<span class="cell header">' + i + '</span>');
}
document.write('<br>');

for (let i = 1; i <= 10; i++) {
    document.write('<span class="cell header">' + i + '</span>');
    for (let j = 1; j <= 10; j++) {
        const mult = i * j
        document.write('<span class="cell">' + mult + '</span>')
    }
    document.write('<br>');
}
